import requests
import xml.etree.ElementTree as ET
import re
import shutil
import os
import io
import zipfile
import time


LAST_VERSION = '2.6.0'
DOWNLOAD_DIRECTORY = 'https://vcvrack.com/downloads/'
SDK_FILENAME_PREFIX = '/downloads/Rack-SDK-'
DOWNLOAD_PREFIX = 'https://vcvrack.com'
SCRIPT_REGEX = re.compile(r'\<script\>.*\</script\>', re.DOTALL | re.IGNORECASE)
STYLE_REGEX = re.compile(r'\<style\>.*\</style\>', re.DOTALL | re.IGNORECASE)
META_REGEX = re.compile(r'\<meta.*\>', re.IGNORECASE)


def get_download_links() -> list[str]:
    source = requests.get(DOWNLOAD_DIRECTORY).text
    source = SCRIPT_REGEX.sub(' ', source)
    source = STYLE_REGEX.sub(' ', source)
    source = META_REGEX.sub(' ', source)
    
    root = ET.fromstring(source)
    results = []
    for elem in root.iter('a'):
        href = elem.attrib.get('href', '')
        if not href.startswith(SDK_FILENAME_PREFIX):
            continue

        results.append(href)
    return results


def extract_versions(links: list[str]) -> dict[str, dict[str, str]]:
    results = dict()
    for link in links:
        version = link[len(SDK_FILENAME_PREFIX):len(SDK_FILENAME_PREFIX)+5]
        if version <= LAST_VERSION or version >= '3.0.0':
            continue

        if version not in results:
            results[version] = dict()

        platform = link[len(SDK_FILENAME_PREFIX)+6:-4]
        platform = platform.replace('%2B', '+')
        results[version][platform] = link

    return results


def get_platform_names(versions: dict[str, dict[str, str]]) -> set[str]:
    results = set()
    for val in versions.values():
        for key in val.keys():
            results.add(key)
    return results


def delete_existing(platforms):
    for platform in platforms:
        if os.path.isdir(platform):
            shutil.rmtree(platform)


def download_version(version, links: dict[str, str]):
    for platform, link in links.items():
        print('Downloading', version, platform)
        os.mkdir(platform)
        data = io.BytesIO(requests.get(DOWNLOAD_PREFIX + link).content)
        zipf = zipfile.ZipFile(data)
        zipf.extractall(platform)
        with open(f'{platform}/Rack-SDK/VERSION', 'w') as f:
            f.write(version + '\n')
    print('Done.')
    


if __name__ == '__main__':
    links = get_download_links()
    versions = extract_versions(links)
    platforms = get_platform_names(versions)
    iter_versions = list(versions.keys())
    iter_versions.sort()

    for version in iter_versions:
        delete_existing(platforms)
        download_version(version, versions[version])
        print('---')
