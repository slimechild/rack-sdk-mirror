# VCV Rack SDK Mirror

Provides a versioned mirror for the VCV Rack SDK available at https://vcvrack.com/downloads/

VCV Rack **source code and binaries** are copyright © 2016-2024 VCV. Please follow the attached license. No additional copyright is claimed by this mirror.
